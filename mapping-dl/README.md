# Image mapping downloader - `mapping-dl.js`

Usage:

    phantomjs mapping-dl.js <mapping file 1> <mapping file 2> ...

# Mapping downloader - `get_mappings.sh`

Dependencies:

 * wget


Usage:

    ./get_mappings <smiles file>

`smiles file` is line by line file with reaction smileses.
