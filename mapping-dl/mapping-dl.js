var webPage = require('webpage');
var system = require('system');
var fs = require('fs');

if (system.args.length === 1) {
	console.log('usage:');
	console.log('\t' + system.args[0] + ' <mapping file 1> <mapping file 2> ...');
	phantom.exit();
}

var downloadMapping = system.args[1] === 'smiles';
var url = 'http://students.mimuw.edu.pl/~mk305158/smiles/branches/origin/janr-atom-mapping-interface-with-new-coordinates/demo/atom_mapping/';
//var url = 'http://localhost:9000/atom_mapping/';
var scale = 10;

var page = webPage.create();
//page.onConsoleMessage = function(msg, lineNum, sourceId) {
//	console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
//};
console.log('opening webpage...', url);
page.open(url, function (status) {
	// make global variables on webpage
	page.evaluate(function () {
		document.body.bgColor = 'white';
		window.scope = angular.element(document.querySelector('[ng-controller=graphController]')).scope();
		window.processMapping = function (mapping) {
			scope.$apply(function () {
				scope.readResponse(mapping);
			});
		};
	});

	// process all input strings
	for (var i = 1; i < system.args.length; i++) {
		console.log('processing', system.args[i]);

		// set input
		var state = page.evaluate(function (mapping) {
			processMapping(mapping);
			return scope.state;
		}, '<root>' + fs.read(system.args[i]) + '</root>');

		var baseName = system.args[i].substring(system.args[i].lastIndexOf('/')+1, system.args[i].lastIndexOf('.'));
		var rasterFile = baseName + '.png';
		var errorFile = baseName + '.log';

		// log error
		if (state !== 'result') {
			var error = page.evaluate(function () {
				return '' + scope.error;
			}, fs.read(system.args[i]));
			console.log('\tstate', state, error);
			fs.write(errorFile, state + ': ' + error + '\n', 'w');
			continue;
		}

		// correct page
		page.evaluate(function (scale) {
			var reaction = document.querySelector('.reaction');
			reaction.setAttribute('style', 'overflow: visible');
		}, scale);

		// save image
		console.log('\tsaving', rasterFile);
		var clipRect = page.evaluate(function () {
			var e = document.querySelector('.reaction');
			var rect = e.getBoundingClientRect();
			var width = e.scrollWidth;
			var height = e.scrollHeight;
			return {
				top: rect.top,
				left: rect.left,
				width: width,
				height: height,
			};
		});
		page.clipRect = clipRect;
		page.render(rasterFile);

	}

	phantom.exit();
});
