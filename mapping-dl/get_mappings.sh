#!/bin/bash

URL=http://students.mimuw.edu.pl/~wjaworski/cgi-bin/chem.cgi

for smilesn in `cat $1`; do
	smiles="`echo -nE "$smilesn" | sed -e 's/\n//g' -e 's/\r//g'`"
	file=`echo -nE "$smiles" | md5sum | cut -f1 -d" "`.xml
	echo "downloading "$file" - "$smiles
	wget -T30 -t1 --post-data "text0=$smiles" --quiet -O $file $URL
done
