var webPage = require('webpage');
var system = require('system');
var fs = require('fs');

var drawSmiles = function (page, smilesString) {
	return page.evaluate(function (smilesString) {
		var scope = angular.element(document.getElementsByTagName('input')[0]).scope();
		scope.$apply(function () {
			scope.input = smilesString;
			scope.update();
		});
		return document.getElementsByTagName('code')[0].textContent;
	}, smilesString);
};

var pad = function (num, size) {
	var s = num + '';
	while (s.length < size) s = '0' + s;
	return s;
}

var tryFixAromaticNitrogen = function (smilesString) {
	smilesString = smilesString.replace(/\[n/g, '[_');
	var parts = smilesString.split('n');
	var nCount = parts.length - 1;
	var possibilitiesCount = Math.pow(2, nCount);
	var possibilities = [pad('', nCount)];
	for (var i = 1; i < possibilitiesCount; i++) {
		possibilities.push(pad(i.toString(2), nCount));
	}
	return possibilities.map(function (possibility) {
		var form = [];
		possibility.split('').forEach(function (nForm, i) {
			form.push(parts[i]);
			form.push(['n', '[nH]'][nForm]);
		});
		form.push(parts[parts.length - 1]);
		return form.join('').replace(/\[_/g, '[n');
	});
}

if (system.args.length === 1) {
	console.error('usage: ' + system.args[0] + ' <smiles1> <smiles2> ...');
	phantom.exit();
}

var url = 'http://students.mimuw.edu.pl/~mk305158/smiles/branches/origin/janr-atom-mapping-interface-with-new-coordinates/demo/graph_proper/';
var maxFilenameLength = 255;
var processedFile = 'processed';
var scale = 10;

// turncate
fs.write(processedFile, '', 'w');

var page = webPage.create();
//page.onConsoleMessage = function(msg, lineNum, sourceId) {
//	console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
//};
console.log('opening webpage...', url);
page.open(url, function start(status) {
	// white page background
	page.evaluate(function () {
		document.body.bgColor = 'white';
	});
	
	for (var i = 1; i < system.args.length; i++) {
		console.log('processing', system.args[i]);
		var smilesString = system.args[i];
		var encodedSmilesString = encodeURIComponent(smilesString);
		if (encodedSmilesString.length > 255 - 4) {
			encodedSmilesString = encodedSmilesString.substring(0, 255 - 4);
		}
		var errorFile = encodedSmilesString + '.log';
		var rasterFile = encodedSmilesString + '.png';

		// set input - fix aromatic n
		var possibleStrings = tryFixAromaticNitrogen(smilesString);
		possibleStrings.push(smilesString);
		var error = 'ups';
		for (var j = 0; j < possibleStrings.length; j++) {
			// set input
			if (j > 0) console.log('\ttrying aromatic n fix', j);
			error = drawSmiles(page, possibleStrings[j]);
			if (error.indexOf('kekulize') === -1) {
				if (j > 0) console.log('\tit worked!', possibleStrings[j]);
				smilesString = possibleStrings[j];
				break;
			}
		}

		// log processed
		fs.write(processedFile, smilesString + '\n', 'a');

		// log error
		if (error !== '') {
			console.log('\terror', error);
			fs.write(errorFile, error + '\n', 'w');
			continue;
		}

		// save image
		console.log('\tsaving', rasterFile);
		var clipRect = page.evaluate(function () {
			var e = document.getElementsByTagName('svg')[0];
			var rect = e.getBoundingClientRect();
			var width = e.scrollWidth;
			var height = e.scrollHeight;
			return {
				top: rect.top,
				left: rect.left,
				width: width,
				height: height,
			};
		});
		page.clipRect = clipRect;
		page.render(rasterFile);
	}

	phantom.exit();
});
