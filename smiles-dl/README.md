# Dependencies

 * phantomjs

# How to use

Usage of `smiles-dl.js` script is

    phantomjs smiles-dl.js <smiles string 1> <smiles string 2> ... <smiles string n>

In current working directory it will create `<smiles>.png` image or `<smiles.log>` plain text file if error ocurred for each supplied smiles string. Additional `processed` file contains line by line list of processed smileses, with applied 'aromatic nitrogen fix'.

Useful command for checking errors

    cat *.log | sort | uniq -c

# Aromatic nitrogen fix

Often instead of `...[nH]...` part in a aromatic ring, there is just `...n...`. This effects in a kekulization error. Smiles downloader tries to guess which of `n` should be `[nH]` by trying all combinations.
